# -*- coding:utf-8 -*-

# 由于二进制1字节是8bit，文本一字节是6bit，将二者转换就是在找6和8的最小公倍数即24bit
# 这大概就是24bit即3二进制字节转换为4文本字节的原因吧
# 教材中：如果要编码的二进制数据不是3的倍数，最后会剩下1个或2个字节怎么办？
# Base64用\x00字节在末尾补足后，再在编码的末尾加上1个或2个=号，
# 表示补了多少字节，解码的时候，会自动去掉
# 那么：因为是三个二进制转4个文本，即可能出现不能转换的情况有两种：差一个二进制字节，即8bit；差两个二进制字节，即16bit
# （1） 差一个二进制8bit才能转成4个文本24bit，也就是说二进制还有4bit有用数据不能查表匹配，那只能用\x00填满，
#       也就是说用\x00填充了2bit后就可以匹配到文本，剩下的6bit证号用来填充一个=
# （2） 差两个二进制16bit才能转成4个文本24bit，也就是说二进制还有2bit有用数据不能查表匹配，那只能用\x00填满，
#       即\x00最终填了4bit，这就可以查表匹配文本。也就是还差12bit，是无用的，所以加2个==来填充

import base64


def safe_base64_decode(s):
    for i in range(len(s) % 4):
        if isinstance(s, bytes):
            s = s + b'='
        else:
            s = s + '='
    return base64.b64decode(s)


def safe_base64_decode_v2(s):
    if isinstance(s, bytes):
        return base64.b64decode(s + b'=' * (len(s) % 4))
    else:
        return base64.b64decode(s + '=' * (len(s) % 4))


if __name__ == '__main__':
    assert b'abcd' == safe_base64_decode(b'YWJjZA=='), safe_base64_decode('YWJjZA==')
    # 断言是正确的
    assert b'abcd' == safe_base64_decode(b'YWJjZA'), safe_base64_decode('YWJjZA')
    print('ok1')  # 断言失败就会报错，且不会执行到这一步
    assert b'abcd' == safe_base64_decode_v2(b'YWJjZA=='), safe_base64_decode_v2('YWJjZA==')
    assert b'abcd' == safe_base64_decode_v2(b'YWJjZA'), safe_base64_decode_v2('YWJjZA')
    print('ok2')
