# -*- coding:utf-8 -*-

import itertools


# 作业
def pi(N, start=1, step=2):
    # 传统的思路
    l = []
    y = 4
    for x in itertools.count(start, step=step):
        if x > N * step:
            break
        l.append(y / x)
        y = y * -1
    return sum(l)


def pi2(N, start=1, step=2):
    n = itertools.cycle([4, -4])
    # n就是4,-4,4,-4.....,由于cycle对象是一个iterator对象，所以可以使用next函数取值
    return sum((next(n) / x) for x in itertools.takewhile(lambda y: y < step * N, itertools.count(start, step=step)))
    # 通过takewhile截取值小于


print(pi(100))
print(pi(1000))
print(pi(10000))
print(pi(100000))
print(pi2(100))
print(pi2(1000))
print(pi2(10000))
print(pi2(100000))
assert 3.04 < pi(10) < 3.05
assert 3.13 < pi(100) < 3.14
assert 3.140 < pi(1000) < 3.141
assert 3.1414 < pi(10000) < 3.1415
assert 3.04 < pi2(10) < 3.05
assert 3.13 < pi2(100) < 3.14
assert 3.140 < pi2(1000) < 3.141
assert 3.1414 < pi2(10000) < 3.1415
print('ok')
