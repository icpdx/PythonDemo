# -*-coding:utf-8 -*-

# Python的hashlib提供了常见的摘要算法，如MD5，SHA1等等。

# 什么是摘要算法呢？摘要算法又称哈希算法、散列算法。它通过一个函数，把任意长度的数据转换为一个长度固定的数据串（通常用16进制的字符串表示）

import hashlib, random

db = {
    'michael': 'e10adc3949ba59abbe56e057f20f883e',
    'bob': '878ef96e86145580c38c87f0410ad153',
    'alice': '99b1c2188db85afee403b1536010c2c9'
}


def login(user, password):
    if user not in db:
        print('user does not exist')
        return False
    md5 = hashlib.md5()
    md5.update(password.encode('utf8'))
    return md5.hexdigest() == db[user]


# 测试1:
assert login('michael', '123456')
assert login('bob', 'abc999')
assert login('alice', 'alice2008')
assert not login('michael', '1234567')
assert not login('bob', '123456')
assert not login('alice', 'Alice2008')
print('ok')

# 测试2
db2 = {}
salt = 'pkVb@Gsv6nkDbUaNd1YSturAzV29VzBrrMH'  # 加密盐匙


def register(username, password):
    db2[username] = get_md5(password + username + salt)


def get_md5(str):
    return hashlib.md5(str.encode('utf8')).hexdigest()


register('uname', 'psw')
print(db2)


# 测试3
class User(object):
    def __init__(self, username, password):
        self.username = username
        self.salt = ''.join([chr(random.randint(48, 122)) for i in range(20)])
        self.password = get_md5(password + self.salt)


db = {
    'michael': User('michael', '123456'),
    'bob': User('bob', 'abc999'),
    'alice': User('alice', 'alice2008')
}


def login2(username, password):
    if username not in db:
        print("user does not exits")
        return False
    user = db[username]
    return user.password == get_md5(password + user.salt)


assert login2('michael', '123456')
assert login2('bob', 'abc999')
assert login2('alice', 'alice2008')
assert not login2('michael', '1234567')
assert not login2('bob', '123456')
assert not login2('alice', 'Alice2008')
print('ok2')
