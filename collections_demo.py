# -*- coding:utf-8 -*-

from collections import namedtuple, deque, defaultdict, OrderedDict, Counter

# nametulp(tuple)
point = namedtuple("point", ['x', 'y'])
p = point(1, 2)
print(p.x, p.y)

# nametulp属于tulpd的子类
print(isinstance(p, point))
print(isinstance(p, tuple))

print("---------华丽分割----------")

# deque
q = deque(['a', 'b', 'c'])
q.append('r')  # 末端添加元素
q.appendleft('l')  # 始端添加元素
print(q)
# deque 还支持pop,popleft

print("---------华丽分割----------")

# defaultdict
# dict在key不存在的时候就会抛出KeyError
# 如果希望key不存在时,返回一个默认值,就可以用defaultdict
# 除了key不存在时返回默认值,defaultdict和dict是完全一样的
dd = defaultdict(lambda: "N/A")
dd[1] = 'start'
print(dd[1])
print(dd[2])

print("---------华丽分割----------")

# OrderedDict
# dict的key是无序的,在对dict做迭代的时,我们无法确定key的顺序
# 如果保持key的顺序,可以用OrderDict
# 记住插入顺序的字典
d = dict([('a', 1), ('b', 2), ('c', 3), ('d', 4), ('e', 5)])
print(d)  # {'a': 1, 'b': 2, 'c': 3}
od = OrderedDict([('a', 1), ('b', 2), ('c', 3), ('d', 4), ('e', 5)])
print(od)


# 理由OderedDict实现一个先进先出的dict,当容量超出限制时,先删除最早添加的key
class LastUpdateOderedDict(OrderedDict):
    def __init__(self, capacity):
        super(LastUpdateOderedDict, self).__init__()
        self.__capacity = capacity

    def __setitem__(self, key, value):
        containsKey = 1 if key in self else 0
        if len(self) - containsKey >= self.__capacity:
            last = self.popitem(last=False)
            print('remove:', last)
        if containsKey:
            del self[key]
            print('set:', (key, value))
        else:
            print('add:', (key, value))
        OrderedDict.__setitem__(self, key, value)

print('FIFO测试')
luod = LastUpdateOderedDict(3)
luod[0] = 'a'
luod[1] = 'b'
luod[2] = 'c'
luod[3] = 'd'
luod[2] = 'x'
print(luod)

print("---------华丽分割----------")

# Counter 计数器
# Counter也是dict子类
c = Counter("当时的我想不到今天,错过的人回不到从前,当时的你坐在我的右手边,梧桐灯下是你清美的侧脸")
print(c)
print(isinstance(c,dict))