import re
from datetime import datetime, timezone, timedelta


def to_timestamp(dt_str, tz_str):
    group = re.match(r'^UTC([\+|-]\d{1,2}):00', tz_str)

    print(datetime.strptime(dt_str, "%Y-%m-%d %H:%M:%S").timestamp())

    # strptime 字符串转换为时间
    # timedelta 计算时间
    # timezone 创建时区
    # replace 强制转换时间
    return datetime.strptime(dt_str, '%Y-%m-%d %H:%M:%S').replace(
        tzinfo=timezone(timedelta(hours=int(group[1])))).timestamp()


print(to_timestamp("2016-03-08 12:00:00", 'UTC+7:00'))
